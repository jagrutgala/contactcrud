﻿using System;
using ContactCRUD.Models;

namespace ContactCRUD.Repository {
    internal class ContactRepo {
        List<Contact> contactList;

        public ContactRepo() {
            this.contactList = new List<Contact>();
            this.contactList.Add(new Contact("aaa", "Aaa", "AAA", "111"));
            this.contactList.Add(new Contact("bbb", "Bbb", "BBB", "222"));
            this.contactList.Add(new Contact("ccc", "Ccc", "CCC", "333"));
            this.contactList.Add(new Contact("ddd", "Ddd", "CCC", "444"));
        }

        private bool Contains(string Number) {
            if (this.contactList.Find(clist => clist.PhoneNumber == Number) != null) {
                return true;
            }
            return false;
        }

        //CREATE
        public void Add(Contact contact) {
            if (this.Contains(contact.PhoneNumber)) {
                throw new ApplicationException("Contact Already Exists");
            }
            contactList.Add(contact);
            Console.WriteLine("Contact Added Successfully");

        }

        public void Insert(int indx, Contact contact) {
            if (this.Contains(contact.PhoneNumber)) {
                throw new ApplicationException("Contact Already Exists");
            }
            contactList.Insert(indx, contact);
        }

        // READ
        public List<Contact> GetAll() {
            return contactList;
        }

        public Contact Get(int indx) {
            if (indx > this.contactList.Count) {
                throw new ApplicationException("Contact Not in List");
            }
            return this.contactList[indx];
        }

        public List<Contact> FilterByCity(string city) {
            return this.contactList.FindAll(c => c.City == city);
        }

        //UPDATE
        public void Update(Contact contact, Contact newContact) {
            if (!this.Contains(contact.PhoneNumber)) {
                throw new ApplicationException("Contact Doesn't Exists");
            }
            if (this.Contains(newContact.PhoneNumber)) {
                throw new ApplicationException("New Contact Already Exists");
            }
            int indx = this.contactList.FindIndex(c => c == contact);
            this.contactList[indx] = newContact;
            Console.WriteLine("Contact Updated Successfully");
        }

        // DELETE
        public void Remove(Contact contact) {
            if (!this.Contains(contact.PhoneNumber)) {
                throw new ApplicationException("Contact Doesn't Exists");
            }
            contactList.Remove(contact);
            Console.WriteLine("Contact Removed Successfully");
        }

        // DISPLAY
        public void DisplayContactList() {
            for (int i = 0; i < this.contactList.Count; i++) {
                Console.WriteLine($"{i} - {this.Get(i)}");
            }
            if (this.contactList.Count == 0) {
                Console.WriteLine("Empty Contact List");
            }
        }

        public void DisplayContact(Contact c) {
            if (c != null) {
                Console.WriteLine(c);
            }
        }

    }
}
