﻿using System;

namespace ContactCRUD.Models {
    internal class Contact {
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }

        public Contact(string name, string address, string city, string phoneNumber) {
            this.Name = name;
            this.Address = address;
            this.City = city;
            this.PhoneNumber = phoneNumber;
        }

        public override string ToString() {
            return $"Name:{this.Name}, Add:{this.Address}, City:{this.City}, Phone:{this.PhoneNumber}";
        }
    }
}
