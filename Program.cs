﻿using ContactCRUD.Constants;
using ContactCRUD.Models;
using ContactCRUD.Repository;

static string Input(string msg) {
    Console.WriteLine(msg);
    return Console.ReadLine();
}

ContactRepo cr = new ContactRepo();
bool isContinue = true;
while (isContinue) {
    // Menu Break
    if (!isContinue) {
        break;
    }

    Console.WriteLine();

    // Menu Display Opertations
    Console.WriteLine($"0> Exit");
    Console.WriteLine($"1> Add Contact");
    Console.WriteLine($"2> Show Contacts");
    Console.WriteLine($"3> Filter Contact");
    Console.WriteLine($"4> Update Contact");
    Console.WriteLine($"5> Delete Contact");

    // Menu Operations Decision
    ContactMenu.MenuOptions choice = (ContactMenu.MenuOptions)int.Parse(Console.ReadLine());
    
    // Menu Perform Operations
    //cr.DisplayContactList();
    switch (choice) {
        #region Add
        case ContactMenu.MenuOptions.Add:
            string nameA = Input("Enter name for new Contact");
            string addressA = Input("Enter address for new Contact");
            string cityA = Input("Enter city for new Contact");
            string phoneNumberA = Input("Enter phone number for new Contact");
            try {
                cr.Add(new Contact(nameA, addressA, cityA, phoneNumberA));
            }
            catch (ApplicationException ex) {
                Console.WriteLine(ex.Message);
                break;
            }

            break;
        #endregion
        #region Show
        case ContactMenu.MenuOptions.Show:
            cr.DisplayContactList();
            break;
        #endregion
        #region Filter
        case ContactMenu.MenuOptions.Filter:
            string cityF = Input("Enter City:");
            foreach (Contact contact in cr.FilterByCity(cityF)) {
                cr.DisplayContact(contact);
            }
            break;
        #endregion
        #region Update
        case ContactMenu.MenuOptions.Update:
            cr.DisplayContactList();
            int indxU = int.Parse(Input("Enter SrNo of Contact you want to Update"));
            Contact contactU;
            try {
                contactU = cr.Get(indxU);
            }
            catch (ApplicationException ex) {
                Console.WriteLine(ex.Message);
                break;
            }
            string addressU = Input("Enter address for new Contact");
            string cityU = Input("Enter city for new Contact");
            try {
            cr.Update(contactU, new Contact(contactU.Name, addressU, cityU, contactU.PhoneNumber));
            }
            catch (ApplicationException ex) {
                Console.WriteLine(ex.Message);
                break;
            }
            break;
        #endregion
        #region Delete
        case ContactMenu.MenuOptions.Delete:
            cr.DisplayContactList();
            int indxD = int.Parse(Input("Enter SrNo of Contact you want to Delete"));
            Contact contactD;
            try {
                contactD = cr.Get(indxD);
                cr.Remove(contactD);
            }
            catch (ApplicationException ex) {
                Console.WriteLine(ex.Message);
                break;
            }
            break;
        #endregion
        case 0:
            isContinue = false;
            break;
        default:
            break;
    }
}